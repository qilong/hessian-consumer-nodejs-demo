var express = require('express');
var router = express.Router();
// var HessianProxy = require('hessian-proxy-xl').Proxy;

/* GET home page. */


/**
 * 测试简单对象传输，返回复杂对象
 */
var ZD = require('dubbozoo');
var zd = new ZD({
    // config the addresses of zookeeper
    conn: '127.0.0.1:2181',
    dubbo: '2.8.4'
});

// connect to zookeeper
zd.connect();
router.get('/', function(req, res, next) {
	zd.getProvider('com.ql.hessian.demo.api.HessianDemoProvider','1.0', function (err, provider) {
		if (err){
			next(err);
		} else {
            provider.invoke("getById", [12], function (err, data) {
                if (err) {
                    next(err);
                }
                res.json(data);
            });
		}
    });
});


/**
 * 测试复杂对象传输，返回简单对象
 */
router.get('/save', function(req, res, next) {
    zd.getProvider('com.ql.hessian.demo.api.HessianDemoProvider','1.0', function (err, provider) {
        if (err){
            next(err);
        } else {
            provider.invoke("saveDemo", [{
                id : 1,
                demo : "你好",
                createTime : new Date()
            }], function (err, data) {
                if (err) {
                    next(err);
                }
                res.json(data);
            });
        }
    });
});

/**
 * 多参数传输，返回集合类型
 */
router.get('/list', function(req, res, next) {
    zd.getProvider('com.ql.hessian.demo.api.HessianDemoProvider','1.0', function (err, provider) {
        if (err){
            next(err);
        } else {
            provider.invoke("getPage", [2,20,"测试"], function (err, data) {
                if (err) {
                    next(err);
                }
                res.json(data);
            });
        }
    });
});

module.exports = router;
